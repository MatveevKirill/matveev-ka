from selenium.webdriver.common.by import By

BUTTON_SIGN_IN = (By.XPATH, "//div[contains(@class, 'responseHead-module-button') and text() = 'Войти']")
BUTTON_AUTH_FORM = (By.XPATH, "//div[contains(@class, 'authForm-module-button') and text() = 'Войти']")
BUTTON_SIGN_OUT = (By.XPATH, "//a[text() = 'Выйти']")
BUTTON_USER_RIGHT_WRAP = (By.XPATH, "//div[contains(@class, 'right-module-mail')]")
BUTTON_CONTACT_SAVE = (By.XPATH, "//button[@data-class-name='Submit']")
BUTTON_PAGE_PROFILE = (By.XPATH, "//a[text() = 'Профиль']")
BUTTON_PAGE_AUDIENCE = (By.XPATH, "//a[text() = 'Аудитории']")

INPUT_USERNAME = (By.NAME, "email")
INPUT_PASSWORD = (By.NAME, "password")
INPUT_CONTACT_BIOGRAPHY = (By.XPATH, "//div[@data-name='fio']/div/input")
INPUT_CONTACT_PHONE = (By.XPATH, "//div[@data-name='phone']/div/input")
INPUT_CONTACT_EMAIL = (By.XPATH, "//div[starts-with(@class, 'js-additional-email')]/div[@class='input']/div/input")

LABEL_USERNAME = (By.XPATH, "//div[contains(@class, 'right-module-userNameWrap')]")
LABEL_AUDIENCE_SEGMENTS = (By.XPATH, "//span[text() = 'Аудиторные сегменты']")
LABEL_CONTACT_INFORMATION = (By.XPATH, "//span[text() = 'Контактная информация']")
LABEL_ERROR_NOTIFY_MODULE = (By.XPATH, "//div[starts-with(@class, 'notify-module-content')]")
LABEL_ERROR_NOTIFY_MODULE_MYCOM = (By.XPATH, "//div[starts-with(@class, 'formMsg')]/div[@class = 'formMsg_text']")
