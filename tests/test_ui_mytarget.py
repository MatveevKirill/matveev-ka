import pytest
from . import base_ui
from ui import locators
from selenium.common import exceptions


class TestCase(base_ui.BaseClass):

    @pytest.mark.UI
    @pytest.mark.usefixtures("login")
    def test_login(self):
        username_text = self.find_element(locator=locators.LABEL_USERNAME)
        text_for_verify = str(username_text.text).lower()

        # Проверка на корректность отображаемых данных о имени пользователя:
        # * Если пользователь не вводил личную информацию (ФИО) в профиле, то будет отображаться 'email';
        # * Если пользователь уже ввёл личные данные (ФИО) в профиле, то они и будут отображаться.
        assert text_for_verify == self.configuration['username'] or text_for_verify == "матвеев кирилл алексеевич", \
            f"Пользователь не совпадает с переданным параметром: '{self.configuration['username']}' != '{str(username_text.text).lower()}' "

    @pytest.mark.UI
    @pytest.mark.usefixtures("login")
    def test_logout(self):
        # Выход из системы.
        self.logout()

        # Проверка ссылки.
        assert self.driver.current_url == "https://target.my.com/", \
            f"Пользователь '{self.configuration['username']}' не вышел из системы."

    @pytest.mark.UI
    @pytest.mark.usefixtures("login")
    def test_edit_profile_data(self):
        # Загрузка страницы.
        self.find_element(locator=locators.BUTTON_PAGE_PROFILE).click()

        # Редактирование информации о пользователе
        self.edit_profile_information(**{
            'biography': "Матвеев Кирилл Алексеевич",
            'phone': "+7 (000) 000-00-00",
            'email': "matveev@plagerxgroup.ru"
        })

        # Обновление данных на странице.
        self.driver.refresh()

        # Asserts.
        assert self.find_element(locator=locators.INPUT_CONTACT_BIOGRAPHY)\
                   .get_attribute('value') == "Матвеев Кирилл Алексеевич"
        assert self.find_element(locator=locators.INPUT_CONTACT_PHONE)\
                   .get_attribute('value') == "+7 (000) 000-00-00"
        assert self.find_element(locator=locators.INPUT_CONTACT_EMAIL)\
                   .get_attribute('value') == "matveev@plagerxgroup.ru"

    @pytest.mark.parametrize("locator_button_name, locator_to_find_element", [
        (locators.BUTTON_PAGE_AUDIENCE, locators.LABEL_AUDIENCE_SEGMENTS),
        (locators.BUTTON_PAGE_PROFILE, locators.LABEL_CONTACT_INFORMATION)
    ])
    @pytest.mark.UI
    @pytest.mark.usefixtures("login")
    def test_load_page(self, locator_button_name, locator_to_find_element):
        # Переход на страницу.
        self.click(locator=locator_button_name)

        try:
            element = self.find_element(locator_to_find_element)
        except exceptions.TimeoutException:
            pytest.fail(f"Переход на страницу не выполнен (locator_button_name='{locator_button_name}', locator_to_find_element='{locator_to_find_element}').")
