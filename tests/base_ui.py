import pytest
from ui import locators
from selenium.common import exceptions
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

COUNT_RETRY_CLICKS = 3


class BaseClass(object):
    driver = None
    configuration = None

    @pytest.fixture(scope='function', autouse=True)
    def setup(self, driver, configuration):
        self.driver = driver
        self.configuration = configuration

    @pytest.fixture(scope='function')
    def login(self):
        # Авторизация на сервисе myTarget.
        self.click(locator=locators.BUTTON_SIGN_IN)

        username = self.find_element(locator=locators.INPUT_USERNAME)
        password = self.find_element(locator=locators.INPUT_PASSWORD)
        username.send_keys(self.configuration['username'])
        password.send_keys(self.configuration['password'])

        self.click(locator=locators.BUTTON_AUTH_FORM)

        # Поиск ошибок при авторизации:
        # * Проверка на ошибку: "Введите email или телефон".
        try:
            message_notify_module_error = self.find_element(locator=locators.LABEL_ERROR_NOTIFY_MODULE)
            pytest.fail(f"Пользователь '{self.configuration['username']}'. Ошибка: {message_notify_module_error.text}")
        except exceptions.TimeoutException:
            pass
        # * Проверка на ошибку на стороннем сайте my.com: "Неверный логин или пароль".
        try:
            message_notify_error_auth_data = self.find_element(locator=locators.LABEL_ERROR_NOTIFY_MODULE_MYCOM)
            pytest.fail(
                f"Пользователь '{self.configuration['username']}'. Ошибка: {message_notify_error_auth_data.text}")
        except exceptions.TimeoutException:
            pass

    def logout(self):
        # Нажимаем на User Drop Down для отображения пункта меню "Выйти".
        self.click(locator=locators.BUTTON_USER_RIGHT_WRAP)

        # Выходим из аккаунта
        self.click(locator=locators.BUTTON_SIGN_OUT)

    def edit_profile_information(self, biography="", phone="", email=""):
        contact_biography = self.find_element(locator=locators.INPUT_CONTACT_BIOGRAPHY)
        contact_phone = self.find_element(locator=locators.INPUT_CONTACT_PHONE)
        contact_email = self.find_element(locator=locators.INPUT_CONTACT_EMAIL)

        # Очистка полей.
        contact_biography.clear()
        contact_phone.clear()
        contact_email.clear()

        # Отправляем новые данные для ввода.
        contact_biography.send_keys(biography)
        contact_phone.send_keys(phone)
        contact_email.send_keys(email)

        self.click(locator=locators.BUTTON_CONTACT_SAVE)

    def find_element(self, locator, timeout=None):
        return self.wait(timeout=timeout).until(EC.presence_of_element_located(locator=locator))

    def wait(self, timeout=None):
        if timeout is None:
            timeout = self.configuration['timeout_loading']
        return WebDriverWait(self.driver, timeout=timeout)

    def click(self, locator, timeout=None):
        for i in list(range(COUNT_RETRY_CLICKS)):
            try:
                self.find_element(locator=locator, timeout=timeout)
                self.wait(timeout=timeout).until(EC.element_to_be_clickable(locator)).click()
                return
            except exceptions.ElementClickInterceptedException or exceptions.StaleElementReferenceException:
                if i == COUNT_RETRY_CLICKS - 1:
                    raise
