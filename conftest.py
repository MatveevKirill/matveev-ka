import pytest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities



def pytest_addoption(parser):
    parser.addoption('--username', default='target@plagerxgroup.ru')
    parser.addoption('--password', default='qWeRtY987')
    parser.addoption('--timeout-load-element', default=3.5)


@pytest.fixture(scope='session')
def configuration(request):
    username = request.config.getoption('--username')
    password = request.config.getoption('--password')
    timeout_loading = int(request.config.getoption('--timeout-load-element'))

    return {
        'username': username,
        'password': password,
        'timeout_loading': timeout_loading
    }


@pytest.fixture(scope='session')
def driver_options():
    profile = webdriver.ChromeOptions()
    profile.add_argument('ignore-certificate-errors')
    return profile


@pytest.fixture(scope='function')
def driver(configuration, driver_options):

    driver = webdriver.Remote("http://127.0.0.1:4444/wd/hub", DesiredCapabilities.CHROME)
    driver.maximize_window()
    driver.get('https://target.my.com/')
    yield driver
    driver.quit()
